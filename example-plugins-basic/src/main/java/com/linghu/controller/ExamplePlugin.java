package com.linghu.controller;

import com.gitee.starblues.bootstrap.SpringPluginBootstrap;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author linghu
 * @date 2024/3/15 9:55
 */
@SpringBootApplication
public class ExamplePlugin extends SpringPluginBootstrap {
    public static void main(String[] args) {
        new ExamplePlugin().run(args);
    }

}
